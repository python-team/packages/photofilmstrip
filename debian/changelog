photofilmstrip (4.1.0-1) unstable; urgency=medium

  * New upstream version 4.1.0
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.7.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sat, 08 Feb 2025 16:11:11 +0100

photofilmstrip (4.0.0-2) unstable; urgency=medium

  * debian/rules: Extended clean target (Closes: #1045979)
  * Updated Standards-Version: 4.6.2 (no changes needed)
  * Updated years in debian/copyright
  * Updated lintian overrides

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Dec 2023 22:58:18 +0100

photofilmstrip (4.0.0-1) unstable; urgency=medium

  [ Philipp Huebner ]
  * New upstream version 4.0.0 (Closes: #1012049)
  * Updated years in debian/copyright
  * Updated debian/source/lintian-overrides

  [ Debian Janitor ]
  * Remove obsolete field Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Update standards version to 4.6.0, no changes needed.

 -- Philipp Huebner <debalance@debian.org>  Wed, 15 Jun 2022 20:07:58 +0200

photofilmstrip (3.7.3-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Philipp Huebner ]
  * Updated lintian overrides
  * d/control: use https for homepage field
  * d/copyright: use https for source field
  * Switched debhelper compat from 12 to 13
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Switched debian/watch to version 4
  * Added debian/lintian-overrides
  * Switched from manual fixes to using dh_sphinxdoc
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 26 Dec 2020 18:31:26 +0100

photofilmstrip (3.7.3-1) unstable; urgency=high

  * New upstream version 3.7.3 (Closes: #955295)

 -- Philipp Huebner <debalance@debian.org>  Wed, 08 Apr 2020 20:52:14 +0200

photofilmstrip (3.7.2-2) unstable; urgency=medium

  * Moved maintainership into Python Applications Packaging Team
  * Updated Vcs-* fields

 -- Philipp Huebner <debalance@debian.org>  Fri, 28 Feb 2020 14:08:39 +0100

photofilmstrip (3.7.2-1) unstable; urgency=medium

  * New upstream version 3.7.2
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Rules-Requires-Root: no
  * Updated debhelper compat version: 12

 -- Philipp Huebner <debalance@debian.org>  Sun, 16 Feb 2020 01:14:25 +0100

photofilmstrip (3.7.1-1) unstable; urgency=medium

  * New upstream version 3.7.1
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.3.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sat, 26 Jan 2019 16:28:07 +0100

photofilmstrip (3.7.0-1) unstable; urgency=medium

  * New upstream version 3.7.0
  * Updated Standards-Version: 4.2.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 18 Nov 2018 19:16:22 +0100

photofilmstrip (3.5.1-2) unstable; urgency=medium

  * Depend on python3-pil (Closes: #904691)
  * Updated Standards-Version: 4.1.5 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Fri, 03 Aug 2018 11:06:41 +0200

photofilmstrip (3.5.1-1) unstable; urgency=medium

  * New upstream version 3.5.1
  * Added debian/upstream/metadata
  * Removed get-orig-source target from debian/rules
  * Updated Standards-Version: 4.1.4 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Fri, 01 Jun 2018 14:48:41 +0200

photofilmstrip (3.4.1-1) unstable; urgency=medium

  * New upstream version 3.4.1 (Closes: #889992)
  * Switched to Python 3 (Closes: #888245) and debhelper compat level 11
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Wed, 21 Feb 2018 14:26:09 +0100

photofilmstrip (3.3.1-1) unstable; urgency=medium

  * New upstream version 3.3.1
  * Updated debian/watch
  * Updated Vcs-* fields in debian/control
  * Updated Standards-Version: 4.1.3 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Dec 2017 22:55:48 +0100

photofilmstrip (3.2.0-1) unstable; urgency=medium

  * New upstream version 3.2.0
  * Remove deprecated patches
  * Remove custom manapges, now provided upstream
  * Remove debian/menu
  * Remove debian/dirs
  * Update debian/copyright
  * Update debian/watch
  * Update debian/control, switch to debhelper 10 and pybuild
  * Update debian/photofilmstrip.doc-base
  * Update dependencies

 -- Philipp Huebner <debalance@debian.org>  Thu, 23 Nov 2017 08:20:43 +0100

photofilmstrip (1.9.92+dfsg-1) unstable; urgency=low

  * New upstream release (Closes: #695062)
  * Updated standards version: 3.9.4 (no changes needed)
  * Updated debian/copyright
  * Corrected debian/watch

 -- Philipp Huebner <debalance@debian.org>  Thu, 18 Jul 2013 13:30:51 +0200

photofilmstrip (1.9.91+dfsg-1) unstable; urgency=low

  [ Gürkan Sengün ]
  * Repackaged (remove windows/)
  * New upstream version (Closes: #668857)
  * Bumped up standards version to 3.9.3.

  [ Philipp Huebner ]
  * Fixed double installation of html documentation
  * Added doc-base registration
  * Added myself as uploader
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Tue, 22 May 2012 14:45:58 +0200

photofilmstrip (1.4.0-1) unstable; urgency=low

  * Initial release. (Closes: #586268)

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 27 Jan 2011 10:26:08 +0100
